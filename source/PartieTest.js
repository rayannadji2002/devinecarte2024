import {Carte} from "./Carte.js";
import {NomCarte} from "./NomCarte.js";
import {Couleur} from "./Couleur.js";
import {Jeu} from "./Jeu.js";
import {Paquet} from "./Paquet.js";
import promptSync from 'prompt-sync';

const prompt = promptSync();
let aide = false ;


// DEMANDE QUEL JEU DE CARTE 32 ou 52

let nbcarte = null;

let paquetDecartes = new Paquet() ;

while (nbcarte != 32 && nbcarte != 52) {
    nbcarte = prompt('Quel jeu de carte voulez-vous celle à 32 cartes ou celle à 52 ?')

    if (nbcarte == 32) {
        paquetDecartes = Paquet.createJeu32Cartes();
        console.log("Création d'un paquet de 32 cartes");
    }
    else if (nbcarte == 52) {
        paquetDecartes = Paquet.createJeu52Cartes();
        console.log("Création d'un paquet de 52 cartes");
    }
}

let rand = Math.trunc(Math.random() * (paquetDecartes.length));
let carterandom = paquetDecartes[rand];

// DEMANDE DE L ASSISTANCE

let question = null;
let aideCouleur = false;
let aideNum = false;
const msgNumGrand = "Le numéro de votre carte est trop grande.";
const msgNumPetit = "Le numéro de votre carte est trop petite.";
const msgNumEgal = "Le numéro de votre carte correspond.";
const msgCouleurDiff = "La couleur de votre carte est différente.";
const msgCouleur = "La couleur de votre carte correspond à la carte du jeux.";

while (question != 'o' && question != 'n') {
    question = prompt("Voulez-vous l aide assisté ? (o)ui/(n)on : ");
    if (question == 'o'){
        aide = true ;
        let a = null;
        let message = "Quelle type d'aide voulez-vous ? : " +
            "\n 1 pour la recherche du numéro." +
            "\n 2 pour la recherche de la couleur." +
            "\n 3 pour les deux." ;
        console.log(message);
        while (a!='1' && a!='2' && a!='3') {
            a = prompt();
            if (a == '1') {
                aideNum = true;
                console.log("Aide à la recherche de numéro activé");
            } else if (a == '2') {
                aideCouleur = true;
                console.log("Aide à la recherche de couleur activé");
            } else if (a == '3') {
                aideNum = true;
                aideCouleur = true;
                console.log("Les deux aides sont activés.");
            } else {
                console.log(message);
            }
        }
        break;
    } else if (question == 'n')
    {
        aide = false ;
        console.log('Aide non activé');
        break;
    }
}





// DEBUT DU JEUX


console.log(" ==== Instanciation du jeu, début de la partie. ====");
//const jeu = new Jeu(aide,paquetDecartes, new Carte(NomCarte.Sept,Couleur.Trefle)) ;
const jeu = new Jeu(aide, paquetDecartes, carterandom) ;
let tentative = 0;


// CARTE CHOISIT PAR L'UTILISATEUR


let carte = null;
let nomCarte = null;
//console.log(nomCarte);

let couleur = null;
let couleurCarte = null;
//console.log(couleurCarte)
// console.log(new Couleur(couleur));

// TODO (A) permettre au joueur de retenter une autre carte (sans relancer le jeu) ou d'abandonner la partie
let retente = 'oui';
let win = false;

while (retente === 'oui' && win === false) {
    console.log(carterandom.toString());
    carte = prompt('Entrez un nom de carte dans le jeu (exemples : Roi, sept, six, As...) :   ');
    nomCarte = NomCarte.getNomCarteFromString(carte) ;

    couleur = prompt('Entrez un nom de \"couleur\" de carte parmi : Pique, Trefle, Coeur, Carreau :  ');
    couleurCarte = Couleur.getCouleurCarteFromString(couleur);

    tentative++;

    if (nomCarte != null && couleurCarte != null) {

        // prise en compte de la carte du joueur
        const carteJoueur = new Carte(nomCarte,couleurCarte) ;

        if (jeu.isMatch(carteJoueur)) {
            console.log("Bravo, vous avez trouvé la bonne carte !");
            win = true;
            break;
        } else {

            console.log("Ce n'est pas bon");
            console.log("Vous avez proposé " + carteJoueur.toString());

            // if (aide == true) {
            //     if (nomCarte.points > jeu.carteADeviner.nomCarte.points) {
            //         console.log("La valeur de votre carte est trop grande.");
            //     }
            //     else if (nomCarte.points == jeu.carteADeviner.nomCarte.points) {
            //         console.log("Vous avez trouvé la bonne valeur mais pas la bonne couleur.");
            //     }
            //     else {
            //         console.log("La valeur de votre carte est trop petite.");
            //     }
            // }

            if (aide) {
                        /* SI LES DEUX AIDES SONT ACTIVES */
                if (aideNum && aideCouleur) {
                    if (carterandom.couleur === couleurCarte) {
                        console.log(msgCouleur);
                        if (nomCarte.points > jeu.carteADeviner.nomCarte.points) {
                            console.log(msgNumGrand);
                        }
                        else if (nomCarte.points == jeu.carteADeviner.nomCarte.points) {
                            console.log(msgNumEgal);
                        }
                        else {
                            console.log(msgNumPetit);
                        }
                    } else {
                        console.log(msgCouleurDiff);
                        if (nomCarte.points > jeu.carteADeviner.nomCarte.points) {
                            console.log(msgNumGrand);
                        }
                        else if (nomCarte.points == jeu.carteADeviner.nomCarte.points) {
                            console.log(msgNumEgal);
                        }
                        else {
                            console.log(msgNumPetit);
                        }
                    }
                        /* FIN DES DEUX AIDES*/

                        /* SI AIDE NUM EST ACTIVE */
                } else if (aideNum == true && aideCouleur == false) {
                    if (nomCarte.points > jeu.carteADeviner.nomCarte.points) {
                        console.log(msgNumGrand);
                    }
                    else if (nomCarte.points == jeu.carteADeviner.nomCarte.points) {
                        console.log(msgNumEgal);
                    }
                    else {
                        console.log(msgNumPetit);
                    }
                        /* SI AIDE COULEUR ACTIVE */
                } else if (aideCouleur == true && aideNum == true) {
                    if (carterandom.couleur == couleurCarte) {
                        console.log(msgCouleur);
                    } else {
                        console.log(msgCouleurDiff);
                    }
                }
            }
        }
    } else {
        console.log("Désolé, mauvaise définition de carte !"+ nomCarte + couleurCarte);
        console.log("Rappel : ");
        if (nbcarte == 32) {
            console.log('Vous avez le choix entre : Sept, Huit, Neuf, Dix, Valet, Dame, Roi, As');
        } else {
            console.log('Vous avez le choix entre : Deux, Trois, Quatre, Cinq, Six, Sept, Huit, Neuf, Dix, Valet, Dame, Roi, As');
        }
        console.log('Et comme couleur : Pique, Trefle, Coeur, Carreau');
        tentative--;
    }

    retente = prompt('Voulez-vous retenter ? (oui ou non) :     ').toLowerCase();
}

console.log('\n==== Fin prématurée de la partie ====');


// TODO (A) Présenter à la fin la carte à deviner

console.log("\n==== Présentation de la carte ====");

console.log(carterandom.toString())

console.log("\n==== Fin de présentation de la carte");

// TODO (challenge-4) la stratégie de jeu est à implémenter... à faire lorsque les autres TODOs auront été réalisés
