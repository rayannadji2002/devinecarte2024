import {Carte} from "./Carte.js";
import {NomCarte} from "./NomCarte.js";
import {Couleur} from "./Couleur.js";
import {Jeu} from "./Jeu.js";
import {Paquet} from "./Paquet.js";
import promptSync from 'prompt-sync';

const prompt = promptSync();
let aide = false ;


// DEMANDE QUEL JEU DE CARTE 32 ou 52

let nbcarte = null;

let paquetDecartes = new Paquet() ;

while (true) {
    nbcarte = prompt('Quel jeu de carte voulez-vous : celui à 32 cartes ou celui à 52 ?');

    if (nbcarte == 32) {
        paquetDecartes = Paquet.createJeu32Cartes();
        console.log("Création d'un paquet de 32 cartes");
        break; // Sortir de la boucle une fois la réponse correcte obtenue
    }
    else if (nbcarte == 52) {
        paquetDecartes = Paquet.createJeu52Cartes();
        console.log("Création d'un paquet de 52 cartes");
        break; // Sortir de la boucle une fois la réponse correcte obtenue
    }
    else {
        alert("Veuillez entrer 32 ou 52."); // Message d'erreur pour une entrée incorrecte
    }
}



let rand = Math.trunc(Math.random() * (paquetDecartes.length));
let carterandom = paquetDecartes[rand];

// DEMANDE DE L ASSISTANCE

let question = null;
let aideCouleur = false;
let aideNum = false;

while (question != 'o' && question != 'n') {
    question = prompt("Voulez-vous l aide assisté ? (o)ui/(n)on : ");
    if (question == 'o'){
        aide = true ;
        let a = null;
        let message = "Quelle type d'aide voulez-vous ? : " +
            "\n 1 pour la recherche du numéro." +
            "\n 2 pour la recherche de la couleur." +
            "\n 3 pour les deux." ;
        console.log(message);
        while (a!='1' && a!='2' && a!='3') {
            a = prompt();
            if (a == '1') {
                aideNum = true;
                console.log("Aide à la recherche de numéro activé");
            } else if (a == '2') {
                aideCouleur = true;
                console.log("Aide à la recherche de couleur activé");
            } else if (a == '3') {
                aideNum = true;
                aideCouleur = true;
                console.log("Les deux aides sont activés.");
            } else {
                console.log(message);
            }
        }
        break;
    } else if (question == 'n')
    {
        aide = false ;
        console.log('Aide non activé');
        break;
    }
}





// DEBUT DU JEUX


console.log(" ==== Instanciation du jeu, début de la partie. ====");
//const jeu = new Jeu(aide,paquetDecartes, new Carte(NomCarte.Sept,Couleur.Trefle)) ;
const jeu = new Jeu(aide, paquetDecartes, carterandom) ;
let tentative = 0;


// CARTE CHOISIT PAR L'UTILISATEUR


let carte = null;
let nomCarte = null;
//console.log(nomCarte);

let couleur = null;
let couleurCarte = null;
//console.log(couleurCarte)
// console.log(new Couleur(couleur));

// TODO (A) permettre au joueur de retenter une autre carte (sans relancer le jeu) ou d'abandonner la partie
let retente = 'oui';
let win = false;

while (retente === 'oui' && win === false) {
    carte = prompt('Entrez un nom de carte dans le jeu (exemples : Roi, sept, six, As...) :?' + carterandom.toString());
    nomCarte = NomCarte.getNomCarteFromString(carte) ;

    couleur = prompt('Entrez un nom de \"couleur\" de carte parmi : Pique, Trefle, Coeur, Carreau : ');
    couleurCarte = Couleur.getCouleurCarteFromString(couleur)

    tentative++;

    if (nomCarte != null && couleurCarte != null) {

        // prise en compte de la carte du joueur
        const carteJoueur = new Carte(nomCarte,couleurCarte) ;

        if (jeu.isMatch(carteJoueur)) {
            console.log("Bravo, vous avez trouvé la bonne carte !");
            win = true;
            break;
        } else {

            console.log("Ce n'est pas bon");
            console.log("Vous avez proposé " + carteJoueur.toString());

            if (aide == true) {
                if (nomCarte.points > jeu.carteADeviner.nomCarte.points) {
                    console.log("La valeur de votre carte est trop grande.");
                }
                else if (nomCarte.points == jeu.carteADeviner.nomCarte.points) {
                    console.log("Vous avez trouvé la bonne valeur mais pas la bonne couleur.");
                }
                else {
                    console.log("La valeur de votre carte est trop petite.");
                }
                // TODO: (A) si l'aide est activée, alors dire si la carte proposée est FAIT
                //  plus petite ou plus grande que la carte à deviner FAIT
            }
        }
    } else {
        console.log("Désolé, mauvaise définition de carte !"+ nomCarte + couleurCarte);
        console.log("Rappel : ");
        if (nbcarte == 32) {
            console.log('Vous avez le choix entre : Sept, Huit, Neuf, Dix, Valet, Dame, Roi, As');
        } else {
            console.log('Vous avez le choix entre : Deux, Trois, Quatre, Cinq, Six, Sept, Huit, Neuf, Dix, Valet, Dame, Roi, As');
        }
        console.log('Et comme couleur : Pique, Trefle, Coeur, Carreau');
        tentative--;
    }

    retente = prompt('Voulez-vous retenter ?').toLowerCase();
}

console.log('\n==== Fin prématurée de la partie ====')


// TODO (A) Présenter à la fin la carte à deviner
console.log(carterandom.toString())



// TODO (challenge-4) la stratégie de jeu est à implémenter... à faire lorsque les autres TODOs auront été réalisés
// if (tentative == 1) {
//     console.log("Bravo du premier coup ! Et sans aide !")
// }
// else if (tentative != 1 && aide == false) {
//     console.log("Bravo vous avez réussi sans aide !")
// }
// else {
//     console.log("Il vous a fallu " + tentative + " tentatives !")
// }
// console.log("Votre stratégie de jeu :"+jeu.strategiePartie())
//
// console.log(" ==== Fin de la partie. ====")