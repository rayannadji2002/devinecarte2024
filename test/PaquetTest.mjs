import {Carte} from '../source/Carte.js';
import assert from 'assert';
import {NomCarte} from "../source/NomCarte.js";
import {Couleur} from "../source/Couleur.js";
import {Paquet} from "../source/Paquet.js";

describe('Paquet', function(){

    describe('taille', function(){
        it('la taille lorsque construit sous forme de liste', function(){
            const paquetCartes = new Paquet(new Array(new Carte(NomCarte.Valet, Couleur.Coeur), new Carte(NomCarte.Dame, Couleur.Pique))) ;
            assert.strictEqual(2, paquetCartes.taille());
        });
        it('test création jeu de 32', function(){
            const paquetCartes = new Paquet(Paquet.createJeu32Cartes()) ;
            assert.strictEqual(paquetCartes.taille(), 32, "La taille n'est pas de 32 cartes");

        });
        it('test création jeu de 52', function(){
            const paquetCartes = new Paquet(Paquet.createJeu52Cartes()) ;

            assert.strictEqual(paquetCartes.taille(), 52, "la taille de mon paquet de 52 n'est pas 52");

        });




    });
    describe('contenu', function(){
        const paquetCartes = new Paquet(new Array(new Carte(NomCarte.Valet, Couleur.Coeur), new Carte(NomCarte.Dame, Couleur.Pique))) ;

        it('le contenu littéraire est', function(){
            assert.equal("Paquet de 2 cartes", paquetCartes.toString());
        });

    });

    describe('melange paquet', function (){
        it('Test de rebattage du paquet de 32 cartes', function (){
            const paquetCarteoriginal = new Paquet(Paquet.createJeu32Cartes()) ;
            const mesCartesOriginal = paquetCarteoriginal.cartes.slice()
            const paquetCartemelanger = paquetCarteoriginal.methodemelanger()
            const mesCartesMelangees =  paquetCartemelanger.cartes

            function isEqual(mesCartesOriginal, mesCartesMelangees) {
                if (mesCartesOriginal.length !== mesCartesMelangees.length) return false

                return mesCartesOriginal.every((value, index) => value === mesCartesMelangees[index])
            }

            let resultat = isEqual(mesCartesOriginal, mesCartesMelangees)

            assert.strictEqual(resultat, false, 'Les cartes sont pas mélangées')
        })

        it('Test de rebattage du paquet de 52 cartes', function (){
            const paquetCarteoriginal = new Paquet(Paquet.createJeu52Cartes()) ;
            const mesCartesOriginal = paquetCarteoriginal.cartes.slice()
            const paquetCartemelanger = paquetCarteoriginal.methodemelanger()
            const mesCartesMelangees =  paquetCartemelanger.cartes

            function isEqual(mesCartesOriginal, mesCartesMelangees) {
                if (mesCartesOriginal.length !== mesCartesMelangees.length) return false

                return mesCartesOriginal.every((value, index) => value === mesCartesMelangees[index])
            }

            let resultat = isEqual(mesCartesOriginal, mesCartesMelangees)

            assert.strictEqual(resultat, false, 'Les cartes sont pas mélangées')
        })
    })
});